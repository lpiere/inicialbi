import React from 'react';
import { BarChart, CartesianGrid, XAxis, YAxis, Tooltip, Legend, Bar, LineChart, Line } from 'recharts';
import { Container, Col, Row, Button } from 'react-bootstrap'

function Visualization(props) {
    console.log(props.ticketMedio)
    return (
        <div>
            <Container>
                <Row>
                    <Col>
                        <h3>Faturamento/Gastos- {props.filial ? props.filial : "Selecione a Filial"}</h3>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <BarChart width={500} height={250} data={props.data}>
                            <CartesianGrid strokeDasharray="3 3" />
                            <XAxis dataKey="dia" />
                            <YAxis />
                            <Tooltip />
                            <Legend />
                            <Bar dataKey="valor" name="Ganhos" fill="#8884d8" />
                            <Bar dataKey="valorCusto" name="Custos" fill="#eb0c0c" />
                        </BarChart>
                    </Col>
                    <Col>
                        <LineChart width={500} height={250} data={props.data}
                            margin={{ top: 5, right: 30, left: 20, bottom: 5 }}>
                            <CartesianGrid strokeDasharray="3 3" />
                            <XAxis dataKey="dia" />
                            <YAxis />
                            <Tooltip />
                            <Legend />
                            <Line type="linear" dataKey="valor" name="Ganhos" stroke="#8884d8" />
                            <Line type="linear" dataKey="valorCusto" name="Custos" stroke="#eb0c0c" />
                        </LineChart>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <h3>Ticket Medio: {props.ticketMedio}</h3>
                    </Col>
                </Row>
            </Container>
        </div>
    );


}
export default Visualization;
