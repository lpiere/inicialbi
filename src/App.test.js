import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<App />, div);
  ReactDOM.unmountComponentAtNode(div);
});

if ('serviceWorker' in navigator) {
  navigator.serviceWorker.register('/public/sw.js')
    .then(function() {
      console.log('SW registered');
    });
}
