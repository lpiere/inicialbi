﻿// require("babel-core").transform("code", options);
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import startsw from './App';
import Visualization from './Visualization/Visualization'
import { Container, Col, Row, Button } from 'react-bootstrap'
import Select from 'react-select'
import SideBar from './SideBar/SideBar'
import $ from "jquery";

startsw();

const filialOptions = [
    { value: '1', label: 'Chapecô' },
    { value: '2', label: 'Toledo' },
    { value: '3', label: 'Cascavel' }
]
const data = [{
    "nome": "Cascavel",
    "valor": [
        {
            "dia": "2018-04-17",
            "valorCusto": 2130.00,
            "valor": 100.00
        },
        {
            "dia": "2018-04-19",
            "valorCusto": 230.00,
            "valor": 300.00
        },
        {
            "dia": "2018-04-20",
            "valorCusto": 100.00,
            "valor": 800.00
        },
        {
            "dia": "2018-04-24",
            "valorCusto": 1000.00,
            "valor": 200.00
        },
        {
            "dia": "2018-04-25",
            "valorCusto": 500.00,
            "valor": 2116.66
        },
        {
            "dia": "2018-04-26",
            "valorCusto": 800.00,
            "valor": 999.00
        },
        {
            "dia": "2018-04-28",
            "valorCusto": 2000.00,
            "valor": 3030.00
        },
    ]
},
{
    "nome": "Toledo",
    "valor": [
        {
            "dia": "2018-03-14",
            "valorCusto": 300.00,
            "valor": 200.00
        },
        {
            "dia": "2018-03-15",
            "valorCusto": 8000.00,
            "valor": 9102.98
        },
        {
            "dia": "2018-03-16",
            "valorCusto": 2000.00,
            "valor": 3334.00
        },
        {
            "dia": "2018-03-19",
            "valorCusto": 2090.00,
            "valor": 998.00
        },
        {
            "dia": "2018-03-24",
            "valorCusto": 200.00,
            "valor": 300.00
        },
        {
            "dia": "2018-03-29",
            "valorCusto": 100.00,
            "valor": 200.00
        }
    ]
}
]

let dataFiltradoPorFilial = [];

const ticketMedio = [
    {
        "nome": "Cascavel",
        "valor": 2500
    },
    {
        "nome": "Toledo",
        "valor": 1500
    }
]
let ticketMedioFiltrado =2000;

class InputCalendario extends React.Component {
    constructor(props) {
        super(props)
    }
    render() {
        return (
            <input type="date" id={this.props.id} />
        )
    }
}

class Filtros extends React.Component {
    constructor(props) {
        super(props)
        this.state = { filial: "", valor: [] }
    }
    render() {
        return (
            <div >
                <Container>
                    <Row>
                        <Col>
                            <label>Filial</label>
                            <Select id="filial" style="width: 500px" options={this.props.filialOptions}
                                onChange={(obj) => {
                                    let json = OnChangeSeletorFilial(obj.label)
                                    dataFiltradoPorFilial = json
                                    this.setState({ filial: obj.label, valor: json })
                                }} />
                        </Col>
                        
                        <Col>
                            <label>Data Inicial</label><br />
                            <InputCalendario id="dataInicial" />
                        </Col>

                        <Col>
                            <label>Data Final</label><br />
                            <InputCalendario id="dataFinal" />
                        </Col>
                        <Col>
                            <br/>
                            <Button bsStyle="success" onClick={() => {
                                let json = OnclickButtonFiltrarData()
                                this.setState({ valor: json })
                            }}>Filtrar Data</Button>
                        </Col>
                    </Row>

                    <div>
                        <PaginaPrincipal filial={this.state.filial} valor={this.state.valor}  />
                    </div>
                </Container>
            </div>
        )
    }
}


class PaginaPrincipal extends React.Component {
    constructor(props) {
        super(props);

        console.log("aqui" + this.props.filial)

        this.state = { valor: [] }


    }



    render() {
        return (
            <div>
                <div>
                    <Visualization filial={this.props.filial} data={this.props.valor} ticketMedio={ticketMedioFiltrado}/>
                </div>
            </div>
        )
    }
}

function pagina() {
    return (
        <div>
            <div>
                <SideBar />
            </div>
            <div>
                <Filtros filialOptions={filialOptions} />
            </div>
        </div>
    )
}

ReactDOM.render(pagina(), document.getElementById('graphics'));

function GetData() {
    return fetch('https://localhost:44337/api/values/cartao')
}

function OnChangeSeletorFilial(text) {
    let json = [];
    console.log(data.length)
    for (let i = 0; i < data.length; i++) {
        if (data[i].nome == text) {
            ticketMedioFiltrado = ticketMedio[i].valor
            json = data[i].valor
        }
    }
    return json
}

function OnclickButtonFiltrarData() {
    let d1 = $('#dataInicial').val().split('-')
    let d2 = $('#dataFinal').val().split('-')


    let dataInicial = new Date(d1[0], d1[1] - 1, d1[2])
    let dataFinal = new Date(d2[0], d2[1] - 1, d2[2])

    let df, dia
    let json = []

    for (let i = 0; i < dataFiltradoPorFilial.length; i++) {
        df = dataFiltradoPorFilial[i].dia.split('-')
        dia = new Date(df[0], df[1] - 1, df[2])

        if (dia >= dataInicial && dia <= dataFinal) 
            json.push(dataFiltradoPorFilial[i])
        
    }

    return json
}